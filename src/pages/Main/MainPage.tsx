import React from 'react'
import Breadcrumb from '../../components/Breadcrumbs'
import { Carousel } from '3d-react-carousal'
import './index.scss'

export const MainPage = () => {
    const slides = [
        <img src='https://picsum.photos/800/300/?random' alt='1' />,
        <img src='https://picsum.photos/800/301/?random' alt='2' />,
        <img src='https://picsum.photos/800/302/?random' alt='3' />,
        <img src='https://picsum.photos/800/303/?random' alt='4' />,
        <img src='https://picsum.photos/800/304/?random' alt='5' />,
    ]

    return (
        <>
            <Breadcrumb links={[
                {
                    url: '/',
                    title: 'Home',
                },
            ]} />
            <section className='content'>
                <h1>Welcome to Smart Calendar!</h1>
                <Carousel slides={slides} autoplay={true} interval={2000} />
                <h2>Lorem ipsum dolor sit amet.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid cumque magni neque porro quasi
                    suscipit velit? Blanditiis culpa cupiditate delectus doloremque eaque eum excepturi exercitationem
                    expedita harum iusto libero minima molestias nam non odit omnis placeat quas, quia recusandae
                    reiciendis sequi suscipit veniam voluptates. Dolor maxime nesciunt officia quos veniam!</p>
                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus delectus dolorum eveniet facilis
                    fugiat harum labore neque nihil quos vel? Accusamus animi, aperiam architecto asperiores assumenda
                    dignissimos distinctio doloribus excepturi, exercitationem expedita laudantium molestias natus,
                    obcaecati quam qui quibusdam quo quod quos sapiente sequi sint temporibus unde vel velit veniam.</p>
                <h2>Lorem ipsum dolor.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aperiam atque consequuntur
                    deleniti dignissimos dolore eos expedita, impedit in, itaque magni necessitatibus numquam
                    praesentium, quidem reprehenderit sequi? Accusantium ad aperiam assumenda consequatur debitis
                    doloremque eos, facilis illo inventore laborum minima molestiae nisi possimus provident quae quaerat
                    quis quos repudiandae rerum, saepe, temporibus tenetur veritatis voluptatibus! At, consequuntur
                    debitis delectus deleniti eligendi, explicabo harum id iste minima necessitatibus nihil rem repellat
                    suscipit temporibus ullam unde veritatis. Dolorum error fuga iste magnam ratione reiciendis suscipit
                    vitae! Assumenda atque dolore ea eaque eius eum excepturi explicabo facere fugiat id in inventore
                    modi mollitia, nam natus nemo officiis pariatur praesentium quas qui quibusdam quis repellendus sit
                    velit voluptates. Eos in labore molestias odit perspiciatis repellat repellendus sequi tenetur.
                    Accusamus at corporis ea earum eveniet incidunt, soluta voluptatem? Amet animi autem beatae dicta
                    doloribus ex fugiat inventore laborum laudantium maiores neque perspiciatis placeat porro quae
                    quidem reiciendis sequi sit, unde vel vitae? Blanditiis dicta dolore ea eos hic odio porro quisquam,
                    saepe suscipit voluptatum. Ab adipisci dolorum explicabo in magnam, modi, mollitia praesentium
                    provident quas, quia rerum sint tenetur voluptatum? Delectus deserunt distinctio, eos explicabo
                    incidunt, maxime modi nam natus reiciendis similique tempore, totam.</p>
            </section>
        </>
    )
}

